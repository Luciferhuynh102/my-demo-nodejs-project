// convert DateTime
let Utils = require("./Utils");
// ==================

// jwt
var jwt = require("jsonwebtoken");
const PRIVATE_KEY = "MIICXgIBAAKBgQDHikastc8+I81zCg/qWW8dMr8mqvXQ3qbPAmu0RjxoZVI47tvs";

// ====================

let routes = function (app, connectionPool) {
  // Create and get projects
  app.get("/", function (req, res) {
    res.render("home");
  });
  // ================================

  // Projects Query

  app.post("/create-projects", function (req, res) {
    let token = req.body.token;
    let member;
    try {
      member = jwt.verify(token, PRIVATE_KEY);
    } catch (error) {
      console.log("ERROR: ", error);
    }
    connectionPool.getConnection(function (err, connection) {
      const project_name = req.body.project_name;
      if (err) {
        console.log("Cannot connect to Database: ", err);
        res.status(422).send("Cannot connect to Database!");
      } else {
        connection.query(`SELECT * FROM projects WHERE members_id=${member.id} AND project_name="${project_name}";`,
          function (err, result, fields) {
            if (result.length == 0) {
              connection.query(`INSERT INTO projects (project_name, created_at, updated_at, members_id) 
                                VALUES ("${project_name}","${Utils.convertDateTimeForSQL()}","${Utils.convertDateTimeForSQL()}","${member.id}");`,
                function (error, result, fields) {
                  if (error) throw error;
                  console.log("====>>>>>>>", `INSERT INTO members_has_projects (members_id,projects_id) VALUES ("${member.id}","${result.insertId}")`)
                  connection.query(`INSERT INTO members_has_projects (members_id,projects_id) VALUES ("${member.id}","${result.insertId}")`, function (err, result) {
                    console.log("create project result: ", result)
                    res.send(result)
                  })
                }
              );
            } else {
              res
                .status(422)
                .send(`Project name: "${project_name}" has been existed`);
            }
          }
        );
        connection.release();
      }
    });
  });



  app.get("/get-projects", function (req, res) {
    try {
      let payload = jwt.verify(req.query.token, PRIVATE_KEY);
      connectionPool.getConnection(function (err, connection) {
        if (err) console.log("Can not connect to Database: ", err);
        connection.query(
          `SELECT * FROM projects WHERE members_id=${payload.id};`,
          function (err, result, fields) {
            if (err) throw err;
            // console.log("projects result: ", result);
            res.send(result);
          }
        );
        connection.release();
      });
    } catch {
      res.status(401).send("Not Authorized.");
    }
  });
  // ===================================
  //Tasks

  app.get("/project/:id/task", function (req, res) {
    res.render("tasks");
  });

  app.get('/get-tasks', function (req, res) {
    // console.log('req.query: ', req.query)
    let projectId = req.query.projectId
    let token = req.query.token
    try {
      member = jwt.verify(token, PRIVATE_KEY)
    } catch (error) {
      res.status(422).send("Not Authenticated.")
    }
    connectionPool.getConnection(function (err, connection) {
      if (err) console.log("Can not connect to Database: ", err)
      connection.query(
        `SELECT * FROM members_has_projects WHERE members_id=${member.id} AND projects_id=${projectId}`,
        function (err, result, fields) {
          let project = result[0]
          if (project) {
            connection.query(
              `SELECT * FROM tasks WHERE project_id=${projectId}`,
              function (err, result) {
                if (err) {
                  console.log('get task err:', err)
                }
                res.send(result)
              })
          } else {
            res.send('Not Authorized.')
          }
        }
      )
      connection.release()
    })
  })

  app.post("/create-tasks", function (req, res) {
    let token = req.body.token;
    let member;
    try {
      member = jwt.verify(token, PRIVATE_KEY)
    } catch (error) {
      console.log("ERROR: ", error)
    }
    connectionPool.getConnection(function (err, connection) {
      const task_name = req.body.task_name;
      const projectId = req.body.projectId;
      const process = req.body.process;
      if (err) {
        console.log("Cannot connect to Database: ", err);
        res.status(422).send("Cannot connect to Database!");
      } else {
        connection.query(
          `SELECT * FROM tasks WHERE project_id="${projectId}" AND task_name="${task_name}";`,
          function (err, results, fields) {
            if (results.length == 0) {
              connection.query(`INSERT INTO tasks (task_name, created_at, updated_at, project_id, process) VALUES ("${task_name}",
              "${Utils.convertDateTimeForSQL()}","${Utils.convertDateTimeForSQL()}","${projectId}","${process}");`,
                function (err, result, fields) {
                  let idTask = result.insertId;
                  if (err) {
                    console.log(err)
                  }
                  console.log(`UPDATE tasks SET position = "${result.insertId}" WHERE id = "${idTask}");`)
                  connection.query(`UPDATE tasks SET position = "${idTask}" WHERE id = "${idTask}");`,
                    function (err, result, fields) {
                      console.log(`INSERT INTO tasks_has_members (members_id, tasks_id) VALUES ("${member.id}","${idTask}")`)
                      connection.query(`INSERT INTO tasks_has_members (members_id, tasks_id) VALUES ("${member.id}","${idTask}")`,
                        function (err, result) {
                          if (err) {
                            console.log(err)
                          }
                          console.log("result tasks has member result:", result)
                          res.send(result)
                        })
                    })
                }
              )
            } else {
              res
                .status(422)
                .send(`Task name: "${task_name}" has been existed`);
            }
          }
        )
      }
    })
  });
  // =======================
  // change position

  // ======================

  app.get("/member", function (req, res) {
    connectionPool.getConnection(function (err, connection) {
      connection.query("SELECT * FROM members", function (err, result, fields) {
        if (err) throw err;
        res.render("members", {
          members: result
        });
      });
      connection.release();
    });
  });
  // ===================

  // Login query

  app.get("/signin", function (req, res) {
    res.render("sign_in");
  });

  app.post("/authenticate", function (req, res) {
    connectionPool.getConnection(function (err, connection) {
      if (err) throw err;
      const body = req.body;
      connection.query(
        `SELECT * FROM members WHERE email="${body.email}" AND password="${
          body.password
        }"`,
        function (err, result, fields) {
          let user = result[0];
          if (user) {
            if (err) {
              console.log(err);
            }
            let token = jwt.sign({
                id: user.id
              },
              PRIVATE_KEY
            );
            res.send({
              token: token,
              name: user.member_name
            });
          } else {
            res.status(422).send("Email or password is not correct");
          }
        }
      );
      connection.release();
    });
  });
  // ====================

  // Register query

  app.get("/register", function (req, res) {
    res.render("sign_up");
  });

  app.post("/create-users", function (req, res) {
    connectionPool.getConnection(function (err, connection) {
      // console.log('err: ', err)
      const body = req.body;
      const member_name = body.member_name;
      const email = body.email;
      const password = body.password;
      var d = new Date();
      if (err) {
        console.log("Cannot connect to Database!", err);
        res.status(422).send("Cannot connect to Database!");
      }
      const query = `INSERT INTO members (member_name, email, password, created_at, updated_at) VALUES("${member_name}", "${email}", "${password}", "${Utils.convertDateTimeForSQL()}", "${Utils.convertDateTimeForSQL()}")`;

      connection.query(query, function (error, result, fields) {
        if (error) {
          if (error.code == "ER_DUP_ENTRY") {
            res.status(422).send({
              err: "Your email has been already registed !"
            });
            return;
          }
        }
        let memberId = result.insertId;
        res.send({
          id: memberId,
          member_name: member_name,
          email: email,
          created_at: Utils.convertDateTimeForSQL(),
          updated_at: Utils.convertDateTimeForSQL()
        });
      });
      connection.release();
    });
  });
};
// ===========================

module.exports = routes;

// app.get("/albums", function (req, res) {
//   let images = [
//     "images/image1.jpg",
//     "images/image2.jpg",
//     "images/image3.jpg",
//     "images/image4.jpg",
//     "images/image5.jpg",
//     "images/image6.jpg"
//   ];
//   res.render("albums", {
//     images: images
//   });
// });

// app.get("/", function (req, res) {
//   connectionPool.getConnection(function (err, connection) {
//     if (err) throw err;
//     connection.query("SELECT * FROM Students", function (err, result, fields) {
//       if (err) throw err;
//       res.send(result);
//     });
//     connection.release();
//   });
// });

// app.get('/users', function (req, res) {
//   connectionPool.getConnection(function (err, connection) {
//     connection.query('SELECT * FROM users', function (err, result, fields) {
//       if (err) throw err
//       res.render('users', {
//         users: result,
//         encode_users: encodeURIComponent(JSON.stringify(result)),
//         name: 'rex'
//       })
//     })
//     connection.release()
//   })
// })

// app.get('/users/:id_user', function (req, res) {
//   connectionPool.getConnection(function (err, connection) {
//     connection.query(`SELECT * FROM users WHERE id=${req.params.id_user}`, function (err, result) {
//       if (err) throw err
//       console.log(result[0])
//       res.render('user', {
//         email: result[0] ? result[0].email : 'Khong co'
//       })
//     })
//     connection.release()
//   })
// })