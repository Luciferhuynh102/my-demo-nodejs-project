let express = require("express");
let exphbs = require("express-handlebars");
let routes = require("./routes");
const PORT = 3000;

let app = express();

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded())

// Parse JSON bodies (as sent by API clients)
app.use(express.json())

let mysql = require("mysql");
app.use(express.static("public"));

app.engine("handlebars", exphbs());
app.set("view engine", "handlebars");

let connectionPool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "password",
  database: "my_project"
});

// let xinTienba = () => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve(1000)
//     }, 3000)
//   })
// }

// let diChoi = async () => {
//   let tienTui = 100
//   // let tienBa
//   // xinTienba().then(rs=> {
//   //   tienBa = rs
//   // })
//   let tienBa = await xinTienba()
// }


routes(app, connectionPool);

// start

let successConnection = function () {
  console.log(`Nodejs server opening on port ${PORT}`);
};

app.listen(PORT, successConnection);