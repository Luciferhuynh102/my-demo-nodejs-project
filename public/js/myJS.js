// check valid password

let check = function () {
    if (document.getElementById('confirm_password').value != '') {
        if (document.getElementById('password').value ==
            document.getElementById('confirm_password').value) {
            document.getElementById('checked_pass').style.display = 'block';
            document.getElementById('invalid_pass').style.display = 'none';
        } else {
            document.getElementById('invalid_pass').style.display = 'block';
            document.getElementById('checked_pass').style.display = 'none';
        }
    } else {
        document.getElementById('checked_pass').style.display = 'none'
        document.getElementById('invalid_pass').style.display = 'none'
    }
}

// =====================

function register() {
    register_form.onsubmit = (event) => {
        event.preventDefault()
        let textWarn = document.getElementById('err_mess');
        let name = document.getElementById('username_register_input').value
        let email = document.getElementById('email_register_input').value
        let password = document.getElementById('password').value

        textWarn.textContent = ''

        if (name != '' && email != '' && password != '') {
            if (document.getElementById('password').value !=
                document.getElementById('confirm_password').value) {
                alert('Please confirm your password again!!!')
            } else {
                axios.post('/create-users', {
                    member_name: name,
                    email: email,
                    password: password
                }).then(rs => {
                    location.href = "/signin"
                }).catch(err => {
                    if (errMess != '') {
                        textWarn.textContent = err.response.data.err
                        textWarn.style.display = 'block'
                    }
                })
            }
        }
    }
}
// ============================

// login function
function login() {
    sign_in_form.onsubmit = (event) => {
        event.preventDefault()
        let textWarn = document.getElementById('err_singin_mess');
        let errMess1 = document.getElementById('email_input').value
        let errMess2 = document.getElementById('password_input').value
        textWarn.textContent = ''
        axios.post('/authenticate', {
            email: email_input.value,
            password: password_input.value
        }).then(rs => {
            localStorage.setItem('user_name', rs.data.name)
            localStorage.setItem('token', rs.data.token)
            location.href = "/"
        }).catch(err => {
            if (errMess1 != '' && errMess2 != '') {
                textWarn.textContent = 'Email or password is not correct'
                textWarn.style.display = 'block'
            }
        })
    }
}
// =====================================